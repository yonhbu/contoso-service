allprojects {
    repositories {
        mavenCentral()
        maven { url "https://repo.spring.io/snapshot" }
        maven { url "https://repo.spring.io/milestone" }
    }
}


subprojects {
    apply plugin: "java"
    apply plugin: "jacoco"
    apply plugin: 'io.spring.dependency-management'

    sourceCompatibility = JavaVersion.VERSION_1_8

    dependencies {
        testImplementation 'org.springframework.boot:spring-boot-starter-test'

        compileOnly "org.projectlombok:lombok:${lombokVersion}"
        annotationProcessor  "org.projectlombok:lombok:${lombokVersion}"
        testCompileOnly  "org.projectlombok:lombok:${lombokVersion}"
        testAnnotationProcessor  "org.projectlombok:lombok:${lombokVersion}"
        implementation platform("org.springframework.boot:spring-boot-dependencies:${springBootVersion}")

        annotationProcessor "org.mapstruct:mapstruct-processor:${mapstructVersion}"
        testAnnotationProcessor "org.mapstruct:mapstruct-processor:${mapstructVersion}"
    }

    ext.libs = [
            unit_tests: [
                    "junit:junit:4.12",
                    "org.mockito:mockito-all:1.10.19",
                    "org.springframework.boot:spring-boot-starter-test:${springBootVersion}",
            ],
            spring_boot: [
                    "org.springframework.boot:spring-boot-starter-web:${springBootVersion}",
                    "org.springframework:spring-context"
            ],
            spring_rest: [
                    "org.springframework.boot:spring-boot-starter-logging:${springBootVersion}",
                    "org.springframework.boot:spring-boot-devtools:${springBootVersion}",
                    "org.springframework.boot:spring-boot-starter-actuator"
            ],
            spring_jpa: [
                    "org.springframework.boot:spring-boot-starter-data-jpa"
            ],
            api_validation:[
                    "org.springframework.boot:spring-boot-starter-validation:${springValidation}"
            ],
            mapstruct:[
                    "org.mapstruct:mapstruct:${mapstructVersion}"
            ],
            mapper:[
                    "org.modelmapper:modelmapper:2.1.1"
            ],
            swagger:[
                    'io.swagger:swagger-annotations:1.6.7'
            ],
            jack:[
                    "com.fasterxml.jackson.core:jackson-annotations:2.10.0",
                    "com.fasterxml.jackson.core:jackson-databind:2.9.7",
                    "com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.9.7"
            ],
            h2_database:[
                    "testImplementation group: 'com.h2database', name: 'h2', version: '1.3.148'"
            ],
            mysql:[
                    "mysql:mysql-connector-java"
            ]
    ]

    test.finalizedBy(project.tasks.jacocoTestReport)

    jacocoTestReport {
        dependsOn test
        reports {
            xml.setRequired true
            xml.setOutputLocation file("${buildDir}/reports/jacoco.xml")
            csv.setRequired false
            html.setOutputLocation file("${buildDir}/reports/jacocoHtml")
        }
    }
}

jacoco {
    toolVersion = "${jacocoVersion}"
    reportsDirectory = file("$rootProject.buildDir/reports")
}


task jacocoMergedReport(type: JacocoReport) {
    dependsOn = subprojects.jacocoTestReport
    additionalSourceDirs.setFrom files(subprojects.sourceSets.main.allSource.srcDirs)
    sourceDirectories.setFrom files(subprojects.sourceSets.main.allSource.srcDirs)
    classDirectories.setFrom files(subprojects.sourceSets.main.output)
    executionData.setFrom project.fileTree(dir: '.', include: '**/build/jacoco/test.exec')
    reports {
        xml.setRequired true
        csv.setRequired false
        html.setRequired true
    }
}

