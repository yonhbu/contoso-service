# (Microservicio Contoso-Service-) Implementando Clean Architecture.

## Environment:

- Plugin Clean Architecture: https://github.com/bancolombia/scaffold-clean-architecture  Es solo una referencia.
- Java version: 8
- SpringBootVersion: 2.4.2
- Gradle version: 6.9
- Intellij Idea (Import the Project)

## Antes de Iniciar

Empezaremos por explicar los diferentes componentes del proyectos y partiremos de los componentes externos, continuando con los componentes core de negocio (dominio) y por �ltimo el inicio y configuraci�n de la aplicaci�n.


# Arquitectura

![Clean Architecture](https://miro.medium.com/max/1400/1*ZdlHz8B0-qu9Y-QO3AXR_w.png)

## Domain

Es el m�dulo m�s interno de la arquitectura, pertenece a la capa del dominio y encapsula la l�gica y reglas del negocio mediante modelos y entidades del dominio.

## Usecases

Este m�dulo gradle perteneciente a la capa del dominio, implementa los casos de uso del sistema, define l�gica de aplicaci�n y reacciona a las invocaciones desde el m�dulo de entry points, orquestando los flujos hacia el m�dulo de entities.

## Infrastructure

### Helpers

En el apartado de helpers tendremos utilidades generales para los Driven Adapters y Entry Points.

### Driven Adapters

Los driven adapter representan implementaciones externas a nuestro sistema, como lo son conexiones a servicios rest,
soap, bases de datos, lectura de archivos planos, y en concreto cualquier origen y fuente de datos con la que debamos
interactuar.

### Entry Points

Los entry points representan los puntos de entrada de la aplicaci�n o el inicio de los flujos de negocio.

## Application

Este m�dulo es el m�s externo de la arquitectura, es el encargado de ensamblar los distintos m�dulos, resolver las dependencias y crear los beans de los casos de use (UseCases) de forma autom�tica, inyectando en �stos instancias concretas de las dependencias declaradas. Adem�s inicia la aplicaci�n (es el �nico m�dulo del proyecto donde encontraremos la funci�n �public static void main(String[] args)�.

**Los beans de los casos de uso se disponibilizan automaticamente gracias a un '@ComponentScan' ubicado en esta capa.**


## TestPlan:
Ejemplo de un Script de pruebas utilizando Postman: Lo encuentras en la raiz del proyecto con el nombre `Contoso_Script_Test.postman_collection`


## Requirimiento 1: Usuario

URL: http://localhost:8000/

El servicio `REST` expone el recurso `/api/v1/usuario` que permite gestionar la Creacion y la Informacion del Usuario de la siguiente manera:

Post solicitud a `/guardar`:
- El codigo de respuesta es 201
- Permite guardar un usuario en la base de datos
- Retorna un mensaje intuitivo al cliente que la operacion se realizo satisfactoriamente

GET solicitud a `/recuperar`:
- El codigo de respuesta es 200
- El cuerpo de la respuesta es un array de registros coincidentes, ordenados por sus ids en orden creciente

GET solicitud a `/consultByCompany/{nombreCompany}`:
- El codigo de respuesta es 200
- Pemite buscar un usuario perteneciente a una company en especifico
- El cuerpo de la respuesta es un objecto con informacion coincidente a la busqueda

PUT solicitud a `{usuarioId}/company/{companyId}/departamento/{departamentoId}`:
- El codigo de respuesta es 200
- Pemite asociar un usuario a una company y departamento en especifico
- Retorna un mensaje intuitivo al cliente que la operacion se realizo satisfactoriamente

DELETE solicitud a `/eliminar/{usuarioId}`:
- El codigo de respuesta es 200
- Pemite eliminar un usuario especifico de la base de datos
- Retorna un mensaje intuitivo al cliente que la operacion se realizo satisfactoriamente


## Requirimiento 2: Company

URL: http://localhost:8000/

El servicio `REST` expone el recurso `/api/v1/company` que permite gestionar la Creacion y la Informacion de Companys de la siguiente manera:


Post solicitud a `/guardar`:
- El codigo de respuesta es 201
- Permite guardar una company en la base de datos
- Retorna un mensaje intuitivo al cliente que la operacion se realizo satisfactoriamente

GET solicitud a `/recuperar`:
- El codigo de respuesta es 200
- El cuerpo de la respuesta es un array de registros coincidentes, ordenados por sus ids en orden creciente



## Requirimiento 3: Departamento

URL: http://localhost:8000/

El servicio `REST` expone el recurso `/api/v1/departamento` que permite gestionar la Creacion y la Informacion de Departamentos de la siguiente manera:

Post solicitud a `/guardar`:
- El codigo de respuesta es 201
- Permite guardar un departamento en la base de datos
- Retorna un mensaje intuitivo al cliente que la operacion se realizo satisfactoriamente

GET solicitud a `/recuperar`:
- El codigo de respuesta es 200
- El cuerpo de la respuesta es un array de registros coincidentes, ordenados por sus ids en orden creciente

PUT solicitud a `{departamentoId}/company/{companyId}`:
- El codigo de respuesta es 200
- Pemite asociar un departamento a una company en especifico
- Permite ademas Modificar un departamento que pertenece a una company en especifico
- Retorna un mensaje intuitivo al cliente que la operacion se realizo satisfactoriamente


## Pruebas unitarias de cobertura:

Para conocer el porcentaje de pruebas unitarias de todo el proyecto ejecute el siguiente comando `gradle jacocoMergedReport` en una consola CMD o Git abierta desde la ra�z del proyecto



