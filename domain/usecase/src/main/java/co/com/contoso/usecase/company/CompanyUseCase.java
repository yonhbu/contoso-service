package co.com.contoso.usecase.company;

import co.com.contoso.model.company.CompanyModel;
import co.com.contoso.model.company.gateways.CompanyRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;


@RequiredArgsConstructor
public class CompanyUseCase {

    private final CompanyRepository companyRepository;


    public CompanyModel guardarCompany (CompanyModel companyModel) {
        return companyRepository.guardarCompany(companyModel);
    }

    public List<CompanyModel> recuperarCompanys () {
        return companyRepository.recuperarCompanys();
    }

    public CompanyModel recuperarCompanyById (Long companyId) {
        return companyRepository.recuperarCompanyById(companyId);
    }
}
