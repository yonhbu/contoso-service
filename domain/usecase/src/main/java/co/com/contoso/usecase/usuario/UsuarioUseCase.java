package co.com.contoso.usecase.usuario;

import co.com.contoso.model.company.CompanyModel;
import co.com.contoso.model.departamento.DepartamentoModel;
import co.com.contoso.model.usuario.UsuarioModel;
import co.com.contoso.model.usuario.gateways.IUsuarioRepository;
import co.com.contoso.usecase.company.CompanyUseCase;
import co.com.contoso.usecase.departamento.DepartamentoUseCase;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class UsuarioUseCase {

    private final IUsuarioRepository iUsuarioRepository;
    private final DepartamentoUseCase departamentoUseCase;
    private final CompanyUseCase companyUseCase;


    public UsuarioModel guardarUsuario(UsuarioModel usuarioModel) {
        return iUsuarioRepository.guardarUsuario(usuarioModel);
    }


    public List<UsuarioModel> recuperarUsuarios() {
        return iUsuarioRepository.recuperarUsuarios();
    }


    public List<UsuarioModel> recuperarUsuarioByNombreCompany (String nombreCompany) {

        return iUsuarioRepository.buscarUsuarioxNombreCompany(nombreCompany);

    }

    public UsuarioModel recuperarUsuarioById(Long usuarioId) {
        return iUsuarioRepository.recuperarUsuarioById(usuarioId);
    }

    public String enrolarUsuarioToCompanyToDepartamento (Long usuarioId, Long companyId, Long departamentoId ) {

        UsuarioModel usuarioEncontrado = this.recuperarUsuarioById(usuarioId);
        DepartamentoModel departamentoEncontrado = departamentoUseCase.recuperarDepartamentoById(departamentoId);
        CompanyModel companyEncontrado = companyUseCase.recuperarCompanyById(companyId);

        usuarioEncontrado.asignarCompany(companyEncontrado);
        usuarioEncontrado.asignarDepartamento(departamentoEncontrado);
        iUsuarioRepository.guardarUsuario(usuarioEncontrado);

        return "�Operacion Exitosa! El Usuario ha sido Enrolar a la Compa�ia "
                + companyEncontrado.getNombre() + " Y al Departamento " + departamentoEncontrado.getDescripcion();
    }



    public String deleteUsuario(Long usuarioId) {
        try {
            return iUsuarioRepository.deleteUsuario(usuarioId);
        } catch (Exception e) {
            return "Usuario No puedo Ser Eliminado";
        }

    }


}
