package co.com.contoso.usecase.departamento;

import co.com.contoso.model.company.CompanyModel;
import co.com.contoso.model.departamento.DepartamentoModel;
import co.com.contoso.model.departamento.gateways.IDepartamentoRepository;
import co.com.contoso.usecase.company.CompanyUseCase;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class DepartamentoUseCase {

    private final IDepartamentoRepository idepartamentoRepository;
    private final CompanyUseCase companyUseCase;


    public DepartamentoModel guardarDepartamento (DepartamentoModel departamentoModel) {
        return idepartamentoRepository.guardarDepartamento(departamentoModel);
    }


    public List<DepartamentoModel> recuperarDepartamentos () {
        return idepartamentoRepository.recuperarDepartamentos();
    }


    public DepartamentoModel recuperarDepartamentoById(Long departamentoId) {
        return idepartamentoRepository.recuperarDepartamentoById(departamentoId);
    }

    public String enrolarDepartamentoToCompany (Long departamentoId, Long companyId) {
        DepartamentoModel departamentoEncontrado = this.recuperarDepartamentoById(departamentoId);
        CompanyModel companyEncontrado = companyUseCase.recuperarCompanyById(companyId);

        departamentoEncontrado.asignarCompany(companyEncontrado);
        idepartamentoRepository.guardarDepartamento(departamentoEncontrado);

        return ("�Operacion Exitosa! El Departamento ha sido Enrolar a la Compa�ia " + companyEncontrado.getNombre());

    }


}
