package co.com.contoso.model.departamento.dto;

import co.com.contoso.model.company.dto.CompanyRsDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@RequiredArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
public class DepartamentoRsDTO {

    private Long idDepartamento;
    private String descripcion;
    private CompanyRsDTO company;

}
