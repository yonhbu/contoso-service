package co.com.contoso.model.usuario.dto;


import co.com.contoso.model.company.dto.CompanyRqDTO;
import co.com.contoso.model.departamento.dto.DepartamentoRqDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@RequiredArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
public class UsuarioRqDTO {

	private String nombre;
	private String apellido;
	private String cargo;
	private String direccion;
	private String telefono;
	private String ciudadResidencia;
	private String estadoUsuario;
	private CompanyRqDTO company;
	private DepartamentoRqDTO departamento;



}
