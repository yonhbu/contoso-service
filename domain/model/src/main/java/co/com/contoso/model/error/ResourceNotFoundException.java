package co.com.contoso.model.error;

public class ResourceNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public ResourceNotFoundException () {
		super(" Parameter Id Not found");
	}

}