package co.com.contoso.model.usuario.gateways;

import co.com.contoso.model.usuario.UsuarioModel;

import java.util.List;

public interface IUsuarioRepository {

    UsuarioModel guardarUsuario (UsuarioModel usuarioModel);
    List<UsuarioModel> recuperarUsuarios ();
    List<UsuarioModel> buscarUsuarioxNombreCompany (String nombreCompany);
    UsuarioModel recuperarUsuarioById(Long usuarioId);
    String deleteUsuario (Long id);


}
