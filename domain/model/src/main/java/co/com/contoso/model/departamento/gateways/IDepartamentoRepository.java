package co.com.contoso.model.departamento.gateways;

import co.com.contoso.model.departamento.DepartamentoModel;

import java.util.List;

public interface IDepartamentoRepository {

    DepartamentoModel guardarDepartamento (DepartamentoModel DepartamentoModel);
    List<DepartamentoModel> recuperarDepartamentos ();
    DepartamentoModel recuperarDepartamentoById (Long departamentoId);

}
