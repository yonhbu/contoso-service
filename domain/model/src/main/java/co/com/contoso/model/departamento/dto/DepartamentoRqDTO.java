package co.com.contoso.model.departamento.dto;

import co.com.contoso.model.company.dto.CompanyRqDTO;
import co.com.contoso.model.usuario.dto.UsuarioRqDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
public class DepartamentoRqDTO {

    private String descripcion;
    private CompanyRqDTO company;
    private List<UsuarioRqDTO> usuarios;

}
