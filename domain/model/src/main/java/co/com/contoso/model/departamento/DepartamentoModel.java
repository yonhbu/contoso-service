package co.com.contoso.model.departamento;

import co.com.contoso.model.company.CompanyModel;
import co.com.contoso.model.usuario.UsuarioModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
public class DepartamentoModel {

    private Long idDepartamento;
    private String descripcion;
    private CompanyModel company;
    private List<UsuarioModel> usuarios;


    public void asignarCompany(CompanyModel companyAsignada) {
        this.company = companyAsignada;
    }
}
