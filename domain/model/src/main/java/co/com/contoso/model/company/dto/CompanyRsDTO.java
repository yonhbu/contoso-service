package co.com.contoso.model.company.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;




@Getter
@Setter
@RequiredArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
public class CompanyRsDTO {

    private Long idCompany;
    private String nombre;
    private String direccion;
    private String ciudadOperacion;


}
