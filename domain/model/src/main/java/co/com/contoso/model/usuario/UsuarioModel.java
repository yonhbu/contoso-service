package co.com.contoso.model.usuario;



import co.com.contoso.model.company.CompanyModel;
import co.com.contoso.model.departamento.DepartamentoModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@RequiredArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
public class UsuarioModel {

    private Long idUsuario;
    private String nombre;
    private String apellido;
    private String cargo;
    private String direccion;
    private String telefono;
    private String ciudadResidencia;
    private String estadoUsuario;
    private CompanyModel company;
    private DepartamentoModel departamento;


    public void asignarCompany(CompanyModel companyAsignada) {
        this.company = companyAsignada;
    }

    public void asignarDepartamento(DepartamentoModel departamentoEncontrado) {
        this.departamento = departamentoEncontrado;
    }
}
