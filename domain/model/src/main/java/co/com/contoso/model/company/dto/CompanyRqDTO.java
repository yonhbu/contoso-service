package co.com.contoso.model.company.dto;

import co.com.contoso.model.departamento.dto.DepartamentoRqDTO;
import co.com.contoso.model.usuario.dto.UsuarioRqDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@RequiredArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
public class CompanyRqDTO {

    private String nombre;
    private String direccion;
    private String ciudadOperacion;
    private List<DepartamentoRqDTO> departamentos;
    private List<UsuarioRqDTO> usuarios;

}
