package co.com.contoso.model.company.gateways;

import co.com.contoso.model.company.CompanyModel;

import java.util.List;

public interface CompanyRepository {

    CompanyModel guardarCompany (CompanyModel CompanyModel);
    List<CompanyModel> recuperarCompanys ();
    CompanyModel recuperarCompanyById (Long idCompany);
}
