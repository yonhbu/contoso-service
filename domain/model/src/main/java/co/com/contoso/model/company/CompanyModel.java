package co.com.contoso.model.company;

import co.com.contoso.model.departamento.DepartamentoModel;
import co.com.contoso.model.usuario.UsuarioModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@RequiredArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
public class CompanyModel {

    private Long idCompany;
    private String nombre;
    private String direccion;
    private String ciudadOperacion;
    private List<DepartamentoModel> departamentos;
    private List<UsuarioModel> usuarios;

}
