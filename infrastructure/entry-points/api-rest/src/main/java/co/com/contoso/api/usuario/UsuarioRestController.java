package co.com.contoso.api.usuario;

import co.com.contoso.api.util.MapStructMapperAPI;
import co.com.contoso.model.usuario.dto.UsuarioRqDTO;
import co.com.contoso.model.usuario.dto.UsuarioRsDTO;
import co.com.contoso.usecase.company.CompanyUseCase;
import co.com.contoso.usecase.departamento.DepartamentoUseCase;
import co.com.contoso.usecase.usuario.UsuarioUseCase;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value = "/api/v1/usuario", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = {"Usuario Service"})
@RequiredArgsConstructor
public class UsuarioRestController {

    private final UsuarioUseCase usuarioUseCase;
    private final DepartamentoUseCase departamentoUseCase;
    private final CompanyUseCase companyUseCase;
    private final MapStructMapperAPI mapstructMapper;


    @PostMapping("/guardar")
    public ResponseEntity<String> guardarUsuario (@Valid @RequestBody UsuarioRqDTO usuarioRqDTO) {

        usuarioUseCase.guardarUsuario (mapstructMapper.usuarioModelToUsuarioRqDTO(usuarioRqDTO));

        return new ResponseEntity<>("ˇOperacion Exitosa! El Usuario ha sido Creado satisfactoriamente", HttpStatus.CREATED);

    }

    @GetMapping("/recuperar")
    public ResponseEntity<List<UsuarioRsDTO>> recuperarUsuarios () {

        List<UsuarioRsDTO> respuestaListaUsuarios = mapstructMapper.mapAllUsuarioModel(usuarioUseCase.recuperarUsuarios());
        return new ResponseEntity<>(respuestaListaUsuarios, HttpStatus.OK);

    }

    @GetMapping("/consultByCompany/{nombreCompany}")
    public ResponseEntity<List<UsuarioRsDTO>> recuperarUsuarioxCompany (@PathVariable ("nombreCompany") String nombreCompany) {

        List<UsuarioRsDTO> respuestaListaUsuariosXCompany = mapstructMapper.mapAllUsuarioModel(usuarioUseCase.recuperarUsuarioByNombreCompany(nombreCompany));
        return new ResponseEntity<>(respuestaListaUsuariosXCompany, HttpStatus.OK);

    }


    @PutMapping("{usuarioId}/company/{companyId}/departamento/{departamentoId}")
    public ResponseEntity<String> enrolarUsuarioToCompanyToDepartamento (@PathVariable Long usuarioId, @PathVariable Long companyId, @PathVariable Long departamentoId) {

        usuarioUseCase.enrolarUsuarioToCompanyToDepartamento(usuarioId,companyId,departamentoId);

        return new ResponseEntity<>(usuarioUseCase.enrolarUsuarioToCompanyToDepartamento(
                usuarioId,companyId,departamentoId),HttpStatus.CREATED);


    }


    @DeleteMapping("/eliminar/{usuarioId}")
    public ResponseEntity<String> delete (@PathVariable("usuarioId") Long usuarioId) {

        return new ResponseEntity<>(usuarioUseCase.deleteUsuario(usuarioId), HttpStatus.OK);
    }



}
