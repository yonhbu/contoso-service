package co.com.contoso.api.departamento;

import co.com.contoso.api.util.MapStructMapperAPI;
import co.com.contoso.model.departamento.dto.DepartamentoRqDTO;
import co.com.contoso.model.departamento.dto.DepartamentoRsDTO;
import co.com.contoso.usecase.departamento.DepartamentoUseCase;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value = "/api/v1/departamento", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = {"Departamento Service"})
@RequiredArgsConstructor
public class DepartamentoRestController {

    private final DepartamentoUseCase departamentoUseCase;
    private final MapStructMapperAPI mapstructMapper;


    @PostMapping("/guardar")
    public ResponseEntity<String> guardarDepartamento(@Valid @RequestBody DepartamentoRqDTO departamentoRqDTO) {

        departamentoUseCase.guardarDepartamento(mapstructMapper.departamentoModelToDepartamentoRqDTO(departamentoRqDTO));

        return new ResponseEntity<>("ˇOperacion Exitosa! El Departamento ha sido Creado Satisfactoriamente", HttpStatus.CREATED);

    }


    @GetMapping("/recuperar")
    public ResponseEntity<List<DepartamentoRsDTO>> recuperarDepartamentos() {

        List<DepartamentoRsDTO> respuestaListaDepartamentos = mapstructMapper.mapAllDepartamentosModel(departamentoUseCase.recuperarDepartamentos());

        return new ResponseEntity<>(respuestaListaDepartamentos, HttpStatus.OK);

    }


    @GetMapping("/recuperar/{id}")
    public ResponseEntity<DepartamentoRsDTO> recuperarDepartamentoById (@PathVariable ("id") Long id) {

        DepartamentoRsDTO departamentoResponse = mapstructMapper.departamentoRsDTOToDepartamentoModel(departamentoUseCase.recuperarDepartamentoById(id));
        return new ResponseEntity<>(departamentoResponse, HttpStatus.OK);

    }


    @PutMapping("{departamentoId}/company/{companyId}")
    public ResponseEntity<String> enrolarDepartamentoToCompany(@PathVariable Long departamentoId, @PathVariable Long companyId) {

        return new ResponseEntity<>( departamentoUseCase.enrolarDepartamentoToCompany(departamentoId,companyId), HttpStatus.CREATED);


    }


}

