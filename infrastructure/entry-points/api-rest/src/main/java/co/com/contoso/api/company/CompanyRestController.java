package co.com.contoso.api.company;

import co.com.contoso.api.util.MapStructMapperAPI;
import co.com.contoso.model.company.dto.CompanyRqDTO;
import co.com.contoso.model.company.dto.CompanyRsDTO;
import co.com.contoso.usecase.company.CompanyUseCase;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/company", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = {"Company Service"})
@RequiredArgsConstructor
public class CompanyRestController {


    private final CompanyUseCase companyUseCase;
    private final MapStructMapperAPI mapstructMapper;


    @PostMapping("/guardar")
    public ResponseEntity<String> guardarCompany (@Valid  @RequestBody CompanyRqDTO companyRqDTO) {

        companyUseCase.guardarCompany(mapstructMapper.companyModelToCompanyRqDTO(companyRqDTO));

        return new ResponseEntity<>("�Operacion Exitosa! La Compa�ia ha sido Creada Satisfactoriamente", HttpStatus.CREATED);

    }


    @GetMapping("/recuperar")
    public ResponseEntity<List<CompanyRsDTO>> recuperarCompany () {

        List<CompanyRsDTO> respuestaListaCompany = mapstructMapper.mapAllCompanyModel(companyUseCase.recuperarCompanys());

        return new ResponseEntity<>(respuestaListaCompany, HttpStatus.OK);

    }

}
