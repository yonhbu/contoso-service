package co.com.contoso.api.util;


import co.com.contoso.model.company.CompanyModel;
import co.com.contoso.model.company.dto.CompanyRqDTO;
import co.com.contoso.model.company.dto.CompanyRsDTO;
import co.com.contoso.model.departamento.DepartamentoModel;
import co.com.contoso.model.departamento.dto.DepartamentoRqDTO;
import co.com.contoso.model.departamento.dto.DepartamentoRsDTO;
import co.com.contoso.model.usuario.UsuarioModel;
import co.com.contoso.model.usuario.dto.UsuarioRqDTO;
import co.com.contoso.model.usuario.dto.UsuarioRsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(
        componentModel = "spring"
)
public interface MapStructMapperAPI {


    @Mapping(target = "idUsuario", ignore = true)
    UsuarioModel usuarioModelToUsuarioRqDTO (UsuarioRqDTO usuarioRqDTO);
    UsuarioRsDTO usuarioRsDTOToUsuarioModel(UsuarioModel usuarioModel);
    List<UsuarioRsDTO> mapAllUsuarioModel (List<UsuarioModel> usuarioModels);


    @Mapping(target = "idCompany", ignore = true)
    CompanyModel companyModelToCompanyRqDTO (CompanyRqDTO companyRqDTO);
    List<CompanyRsDTO> mapAllCompanyModel (List<CompanyModel> CompanyModels);

    @Mapping(target = "idDepartamento", ignore = true)
    DepartamentoModel departamentoModelToDepartamentoRqDTO (DepartamentoRqDTO departamentoRqDTO);
    List<DepartamentoRsDTO> mapAllDepartamentosModel (List<DepartamentoModel> departamentoModels);
    DepartamentoRsDTO departamentoRsDTOToDepartamentoModel(DepartamentoModel departamentoModel);



}