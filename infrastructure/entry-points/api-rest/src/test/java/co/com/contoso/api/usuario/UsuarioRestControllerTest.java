package co.com.contoso.api.usuario;

import co.com.contoso.model.usuario.UsuarioModel;
import co.com.contoso.model.usuario.dto.UsuarioRqDTO;
import co.com.contoso.model.usuario.dto.UsuarioRsDTO;
import co.com.contoso.usecase.usuario.UsuarioUseCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;


public class UsuarioRestControllerTest {

    @InjectMocks
    private UsuarioRestController mockUsuarioRestController;

    @Mock
    private UsuarioUseCase mockUsuarioUseCase;

    private static Long ID = 1L;

    UsuarioModel usuarioModel;
    UsuarioRqDTO usuarioRqDTO;
    UsuarioRsDTO usuarioRsDTO;
    private List<UsuarioModel> listaUsuarios = new ArrayList<>();



    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        usuarioRqDTO = UsuarioRqDTO.builder().nombre("Juan").apellido("Vargas")
                .cargo("jDeveloper").direccion("Carrera 26")
                .telefono("31358260606").ciudadResidencia("Medellin").estadoUsuario("Activo").build();

        usuarioRsDTO = UsuarioRsDTO.builder().idUsuario(1L).nombre("Juan").apellido("Vargas")
                .cargo("jDeveloper").direccion("Carrera 26")
                .telefono("31358260606").ciudadResidencia("Medellin").estadoUsuario("Activo").build();

        listaUsuarios.add(usuarioModel);

        usuarioModel = UsuarioModel.builder().nombre("Juan").apellido("Vargas")
                                                        .cargo("jDeveloper").direccion("Carrera 26")
                                                        .telefono("31358260606").ciudadResidencia("Medellin").estadoUsuario("Activo").build();


        when(mockUsuarioUseCase.guardarUsuario(usuarioModel)).thenReturn(usuarioModel);
        when(mockUsuarioUseCase.recuperarUsuarios()).thenReturn(listaUsuarios);

    }


    @Test
    public void debeGuardarUsuario() {

    }


    @Test
    public void debeRecuperarTodoslosUsuarios() {


    }


    @Test
    public void debeEliminarUsuarioPorId () {

        when(mockUsuarioUseCase.deleteUsuario(ID)).thenReturn("Usuario Borrado Exitosamente");
        ResponseEntity<String> response;
        response = mockUsuarioRestController.delete(ID);
        assertNotNull(response);
        System.out.println("*************");
        System.out.println(response.getBody());


    }



}



