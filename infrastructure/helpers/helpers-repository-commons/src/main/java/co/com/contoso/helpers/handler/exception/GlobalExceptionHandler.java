package co.com.contoso.helpers.handler.exception;

import co.com.contoso.helpers.handler.exception.dto.MessageErrorArgumentNotValidDTO;
import co.com.contoso.helpers.handler.exception.dto.MessageErrorConstraintViolationDTO;
import co.com.contoso.helpers.handler.exception.dto.MessageErrorDTO;
import co.com.contoso.model.error.ResourceNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(value = {ResourceNotFoundException.class})
    public final ResponseEntity<Object> handleMethodIdNotFound(RuntimeException ex, WebRequest request) {
        MessageErrorDTO responseError = new MessageErrorDTO(
                new Date(),
                HttpStatus.NOT_FOUND.getReasonPhrase(),
                ex.getMessage(),
                request.getDescription(false));

        return handleExceptionInternal(ex, responseError, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }



    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers,
                                                                  final HttpStatus status, final WebRequest request) {
        MessageErrorArgumentNotValidDTO errorDTO = new MessageErrorArgumentNotValidDTO(HttpStatus.BAD_REQUEST, "Validation error");

        errorDTO.setDetailedMessages(ex.getBindingResult().getAllErrors().stream()
                .map(err -> err.unwrap(ConstraintViolation.class))
                .map(err -> String.format("'%s' %s", err.getPropertyPath(), err.getMessage()))
                .collect(Collectors.toList()));

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDTO);
    }


    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException e) {
        List<String> details;

        details = e.getConstraintViolations()
                .stream()
                .map(error -> error.getPropertyPath() + " : " + error.getMessage())
                .collect(Collectors.toList());


        MessageErrorConstraintViolationDTO responseError = new MessageErrorConstraintViolationDTO(
                new Date(),
                HttpStatus.BAD_REQUEST.value(),
                "Validation Errors",
                details);

        return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);

    }


    @ExceptionHandler(value = IllegalArgumentException.class)
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException e) {
        Map<String, Object> errorInfo = new HashMap<>();
        errorInfo.put("message", e.getMessage());
        errorInfo.put("status", HttpStatus.BAD_REQUEST);
        errorInfo.put("status_code", HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(errorInfo, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public ResponseEntity<Object> handleSQLIntegrityConstraintViolation(SQLIntegrityConstraintViolationException e, WebRequest request) {

        MessageErrorDTO responseError = new MessageErrorDTO(
                new Date(),
                HttpStatus.BAD_REQUEST.getReasonPhrase(),
                e.getMessage(),
                request.getDescription(false));

        return handleExceptionInternal(e, responseError, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

}