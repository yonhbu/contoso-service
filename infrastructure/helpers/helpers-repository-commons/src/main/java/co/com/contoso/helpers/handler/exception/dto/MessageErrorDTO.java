package co.com.contoso.helpers.handler.exception.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
public class MessageErrorDTO {
	
    private Date timestamp;
	private String status;
	private String message;
	private String details;
}
