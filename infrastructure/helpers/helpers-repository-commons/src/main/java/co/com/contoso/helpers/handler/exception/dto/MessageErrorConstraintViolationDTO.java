package co.com.contoso.helpers.handler.exception.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
public class MessageErrorConstraintViolationDTO {

	private final Date date;
	private final int status;
	private final String error;
	private List<String> detailedMessages;


}
