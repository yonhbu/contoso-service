package co.com.contoso.helpers.handler.exception.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import javax.persistence.Entity;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
public class MessageErrorArgumentNotValidDTO {

	private final int status;
	private final String error;
	private final String message;
	private List<String> detailedMessages;

	public MessageErrorArgumentNotValidDTO(HttpStatus httpStatus, String message) {
		status = httpStatus.value();
		error = httpStatus.getReasonPhrase();
		this.message = message;
	}

}
