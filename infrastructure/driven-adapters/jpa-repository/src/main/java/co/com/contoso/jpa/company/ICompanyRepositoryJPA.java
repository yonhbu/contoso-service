package co.com.contoso.jpa.company;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ICompanyRepositoryJPA extends CrudRepository<CompanyEntityJPA, Long> {


		
}
