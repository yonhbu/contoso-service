package co.com.contoso.jpa.util;


import co.com.contoso.jpa.company.CompanyEntityJPA;
import co.com.contoso.jpa.departamento.DepartamentoEntityJPA;
import co.com.contoso.jpa.usuario.UsuarioEntityJPA;
import co.com.contoso.model.company.CompanyModel;
import co.com.contoso.model.departamento.DepartamentoModel;
import co.com.contoso.model.usuario.UsuarioModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring"
)
public interface MapStructMapperJPA {


    UsuarioEntityJPA usuarioEntityToUsuarioModel (UsuarioModel usuarioModel);
    UsuarioModel usuarioModelToUsuarioEntity (UsuarioEntityJPA usuarioEntityJPA);
    List<UsuarioModel> mapAllUsuarioModel (Iterable<UsuarioEntityJPA> usuarioEntityJPA);
    UsuarioModel userModelToUserEntityWithOptional (Optional<UsuarioEntityJPA> userModelEntityJPA);

    DepartamentoModel departamentoModelToDepartamentoEntity (DepartamentoEntityJPA departamentoEntityJPA);
    DepartamentoEntityJPA departamentoEntityToDepartamentoModel (DepartamentoModel departamentoModel);


    List<DepartamentoModel> mapAllDepartamentoModel (Iterable<DepartamentoEntityJPA> departamentoEntityJPA);



    //DepartamentoModel departamentoModelToDepartamentoEntityWithOptional (Optional<DepartamentoEntityJPA> departamentoEntityJPA);


   // CompanyModel companytoModelToCompnayEntityWithOptional (Optional<CompanyEntityJPA> departamentoEntityJPA);



}