package co.com.contoso.jpa.company;


import co.com.contoso.helpers.mapper.ObjectMapperUtils;
import co.com.contoso.model.company.CompanyModel;
import co.com.contoso.model.company.gateways.CompanyRepository;
import co.com.contoso.model.error.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Component
@RequiredArgsConstructor
public class CompanyEntityOperationJPA implements CompanyRepository {

	private final ICompanyRepositoryJPA iCompanyRepositoryJPA;


	@Override
	public CompanyModel guardarCompany(CompanyModel companyModel) {

		CompanyEntityJPA companyRequest = ObjectMapperUtils.map(companyModel, CompanyEntityJPA.class);
		CompanyEntityJPA companyDataJPA = iCompanyRepositoryJPA.save(companyRequest);
		return ObjectMapperUtils.map(companyDataJPA, CompanyModel.class);

	}

	@Override
	@Transactional(readOnly = true)
	public List<CompanyModel> recuperarCompanys() {

		List<CompanyEntityJPA> listCompanyDataJPA = (List<CompanyEntityJPA>) iCompanyRepositoryJPA.findAll();
		return ObjectMapperUtils.mapAll(listCompanyDataJPA, CompanyModel.class);

	}

	@Override
	public CompanyModel recuperarCompanyById(Long idCompany) {

		Optional<CompanyEntityJPA> companyResponse = iCompanyRepositoryJPA.findById(idCompany);
		if (!companyResponse.isPresent()) {
			throw new ResourceNotFoundException ();
		}
		return ObjectMapperUtils.map(companyResponse.get(), CompanyModel.class);
	}

}

