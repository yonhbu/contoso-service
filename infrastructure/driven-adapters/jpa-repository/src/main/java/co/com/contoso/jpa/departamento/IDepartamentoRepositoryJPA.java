package co.com.contoso.jpa.departamento;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface IDepartamentoRepositoryJPA extends CrudRepository<DepartamentoEntityJPA, Long> {

}
