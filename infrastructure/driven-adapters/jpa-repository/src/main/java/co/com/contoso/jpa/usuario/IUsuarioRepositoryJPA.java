package co.com.contoso.jpa.usuario;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface IUsuarioRepositoryJPA extends CrudRepository<UsuarioEntityJPA, Long> {

    @Query("SELECT u FROM UsuarioEntityJPA u WHERE u.company.nombre = :nombre")
    List<UsuarioEntityJPA> findByIdUsuario (@Param("nombre") String nombre);
		
}
