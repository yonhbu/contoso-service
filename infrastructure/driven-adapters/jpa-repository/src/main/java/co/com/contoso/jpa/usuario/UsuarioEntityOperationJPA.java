package co.com.contoso.jpa.usuario;


import co.com.contoso.helpers.mapper.ObjectMapperUtils;
import co.com.contoso.jpa.util.MapStructMapperJPA;
import co.com.contoso.model.error.ResourceNotFoundException;
import co.com.contoso.model.usuario.UsuarioModel;
import co.com.contoso.model.usuario.gateways.IUsuarioRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Component
@RequiredArgsConstructor
public class UsuarioEntityOperationJPA implements IUsuarioRepository {

	private final IUsuarioRepositoryJPA iUsuarioRepositoryJPA;
	private final MapStructMapperJPA mapstructMapper;


	@Override
	public UsuarioModel guardarUsuario(UsuarioModel usuarioModel) {
		return mapstructMapper.usuarioModelToUsuarioEntity(
				iUsuarioRepositoryJPA.save(mapstructMapper.usuarioEntityToUsuarioModel(usuarioModel)));
	}

	@Override
	@Transactional(readOnly = true)
	public List<UsuarioModel> recuperarUsuarios() {

		return mapstructMapper.mapAllUsuarioModel(iUsuarioRepositoryJPA.findAll());
	}


	@Override
	public List<UsuarioModel> buscarUsuarioxNombreCompany(String nombre) {

		List<UsuarioEntityJPA> listCompanyDataJPA = iUsuarioRepositoryJPA.findByIdUsuario(nombre);
		return ObjectMapperUtils.mapAll(listCompanyDataJPA, UsuarioModel.class);
	}

	@Override
	@Transactional(readOnly = true)
	public UsuarioModel recuperarUsuarioById(Long usuarioId) {
		Optional<UsuarioEntityJPA> usuarioResponse = iUsuarioRepositoryJPA.findById(usuarioId);
		if (!usuarioResponse.isPresent()) {
			throw new ResourceNotFoundException();
		}
		return ObjectMapperUtils.map(usuarioResponse.get(), UsuarioModel.class);
	}


	@Override
	public String deleteUsuario(Long id) {
		try {
			iUsuarioRepositoryJPA.deleteById(id);
			return "Usuario Borrado Exitosamente";
		} catch (Exception e) {
			return "Usuario No puedo Ser Eliminado";
		}

	}

}
