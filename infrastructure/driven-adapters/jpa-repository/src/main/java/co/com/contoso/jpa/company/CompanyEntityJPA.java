package co.com.contoso.jpa.company;

import co.com.contoso.jpa.departamento.DepartamentoEntityJPA;
import co.com.contoso.jpa.usuario.UsuarioEntityJPA;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="COMPANY")
public class CompanyEntityJPA implements Serializable {

    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_Company")
    @Id
    private Long idCompany;

    @NotBlank(message = "no deberia estar vacio")
    @Column (name = "Nombre")
    private String nombre;

    @NotBlank(message = "no deberia estar vacio")
    @Column (name = "Direccion")
    private String direccion;

    @NotBlank(message = "no deberia estar vacio")
    @Column (name = "Ciudad_Operacion")
    private String ciudadOperacion;

    @Transient
    @OneToMany(mappedBy = "company")
    private List<DepartamentoEntityJPA> departamentos;

    @Transient
    @OneToMany(mappedBy = "company")
    private List<UsuarioEntityJPA> usuarios;


}
