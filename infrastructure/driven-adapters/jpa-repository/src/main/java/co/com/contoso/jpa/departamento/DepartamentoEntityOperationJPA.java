package co.com.contoso.jpa.departamento;


import co.com.contoso.helpers.mapper.ObjectMapperUtils;
import co.com.contoso.jpa.util.MapStructMapperJPA;
import co.com.contoso.model.departamento.DepartamentoModel;
import co.com.contoso.model.departamento.gateways.IDepartamentoRepository;
import co.com.contoso.model.error.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Component
@RequiredArgsConstructor
public class DepartamentoEntityOperationJPA implements IDepartamentoRepository {

	private final IDepartamentoRepositoryJPA iDepartamentoRepositoryJPA;
	private final MapStructMapperJPA mapstructMapper;


	@Override
	public DepartamentoModel guardarDepartamento(DepartamentoModel departamentoModel) {

		DepartamentoEntityJPA departamentoRequest = ObjectMapperUtils.map(departamentoModel, DepartamentoEntityJPA.class);
		DepartamentoEntityJPA departamentoDataJPA = iDepartamentoRepositoryJPA.save(departamentoRequest);
		return ObjectMapperUtils.map(departamentoDataJPA, DepartamentoModel.class);

	}

	@Override
	@Transactional(readOnly = true)
	public List<DepartamentoModel> recuperarDepartamentos() {
		return mapstructMapper.mapAllDepartamentoModel(iDepartamentoRepositoryJPA.findAll());
	}


	@Override
	@Transactional(readOnly = true)
	public DepartamentoModel recuperarDepartamentoById (Long departamentoId) {

		Optional<DepartamentoEntityJPA> departamentoResponse = iDepartamentoRepositoryJPA.findById(departamentoId);
		if (!departamentoResponse.isPresent()) {
			throw new ResourceNotFoundException ();
		}
		return ObjectMapperUtils.map(departamentoResponse.get(), DepartamentoModel.class);
	}


}
