package co.com.contoso.jpa.usuario;


import co.com.contoso.jpa.company.CompanyEntityJPA;
import co.com.contoso.jpa.departamento.DepartamentoEntityJPA;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="USUARIO")
public class UsuarioEntityJPA implements Serializable {

	private static final long serialVersionUID = 1L;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name = "id_Usuario")
	@Id
	private Long idUsuario;

	@NotBlank(message = "no deberia estar vacio")
	@Column (name = "Nombre")
	private String nombre;

	@NotBlank(message = "no deberia estar vacio")
	@Column (name = "Apellido")
	private String apellido;

	@NotBlank(message = "no deberia estar vacio")
	@Column (name = "Cargo")
	private String cargo;

	@NotBlank(message = "no deberia estar vacio")
	@Column (name = "Direccion")
	private String direccion;

	@NotBlank(message = "no deberia estar vacio")
	@Column (name = "Telefono")
	private String telefono;

	@NotBlank(message = "no deberia estar vacio")
	@Column (name = "Ciudad_Residencia")
	private String ciudadResidencia;

	@Column (name = "Estado_Usuario")
	private Boolean estadoUsuario;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_id_Company", referencedColumnName = "id_Company")
	private CompanyEntityJPA company;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_id_Departamento", referencedColumnName = "id_Departamento")
	private DepartamentoEntityJPA departamento;


}
