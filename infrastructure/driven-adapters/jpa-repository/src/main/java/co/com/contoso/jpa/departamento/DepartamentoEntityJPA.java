package co.com.contoso.jpa.departamento;

import co.com.contoso.jpa.company.CompanyEntityJPA;
import co.com.contoso.jpa.usuario.UsuarioEntityJPA;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="DEPARTAMENTO")
public class DepartamentoEntityJPA implements Serializable {

    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_Departamento")
    @Id
    private Long idDepartamento;

    @NotBlank(message = "no deberia estar vacio")
    @Column (name = "Descripcion")
    private String descripcion;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_id_Company", referencedColumnName = "id_Company")
    private CompanyEntityJPA company;

    @Transient
    @OneToMany(mappedBy = "departamento")
    private List<UsuarioEntityJPA> usuarios;


}
